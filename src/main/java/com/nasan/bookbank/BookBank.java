/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.bookbank;

/**
 *
 * @author nasan
 */
public class BookBank {
    String name;
    double balance;
    
    //Constructor method
    BookBank(String name, double money){
        this.name = name;
        this.balance = money;
    }
    
    void deposit(double money){
        if (money <= 0){
            System.out.println("Incorrect number");
            return;
        }
        //this. is for using a variable for this method. 
        this.balance = this.balance + money;    // In this case this. mean book1 or book2.
    }
    
    void widthdraw(double money){
        //Not normal case
        if (money <= 0){
            System.out.println("Incorrect number");
            return;
        }
        //Not normal case
        if (money>balance){
            System.out.println("Not enough money to widthdraw");
            return;//Use return to stop doing this method.
        }
        //Normal case
        this.balance = this.balance - money;
        
        //By using not nornal case first and put normal case to the last
        //make it's more easier to understand when method has a lot of cases.  
    }
}
